let students = [];

const addStudent = (group, name, gender, birthdate, status) => {
  students.unshift({
    group: group,
    name: name,
    gender: gender,
    birthdate: birthdate,
    status: status,
  });
};

let curPage = 0;
const sizeOfPage = 5;

let indexToDelete = null;

const renderTable = () => {
  console.log("rendered");
  const table = document.getElementById("student-table");
  table.childNodes = new Array();
  let rows = `
  <tr>
    <th><input type="checkbox" /></th>
    <th>Group</th>
    <th>Name</th>
    <th>Gender</th>
    <th>Birthday</th>
    <th>Status</th>
    <th>Options</th>
  </tr>
  `;
  const dif = students.length - curPage * sizeOfPage;
  const length = dif < sizeOfPage ? dif : sizeOfPage;
  const start = curPage * sizeOfPage;
  for (let i = start; i < start + length; i++) {
    rows += `
    <tr>
      <td><input type = "checkbox"/></td>
      <td>${students[i].group}</td>
      <td>${students[i].name}</td>
      <td>${students[i].gender}</td>
      <td>${students[i].birthdate}</td>
      <td>${students[i].status}</td>
      <td><button>${"/"}</button><button onclick = "callDeletionForm(event)">x</button></td>
    </tr>
    `;
  }
  table.innerHTML = rows;
};

const callCreationForm = () => {
  const app = document.getElementById("app");
  const old = app.innerHTML;
  app.innerHTML = `
    <div class = "backdrop" onclick = "closeForm()"></div>
    <div class = "overlay">
      <header>
        <h2>Add Student</h2>
        <button onclick = "closeForm()">x</button>
      </header>
      <div class = "info">
        <label for = "group-input">Group</label>
        <input id = "group-input"/>
        <label for = "first-name-input">First name</label>
        <input id = "first-name-input"/>
        <label for = "last-name-input">Last name</label>
        <input id = "last-name-input"/>
        <label for = "gender-input">Gender</label>
        <input id = "gender-input"/>
        <label for = "birthdate-input">Birthdate</label>
        <input id = "birthdate-input"/>
      </div>
      <div class = "actions">
        <button onclick = "createStudent(), closeForm()">create</button>
      </div>
    </div>${old}`;
};

const deleteStudent = () => {
  students = students.filter(
    (item) => item.name != students[indexToDelete].name
  );
  if (students.length == sizeOfPage) {
    const tableContainer = document.getElementsByClassName("grid-container")[0];
    const pageChanger = document.getElementById("page-changer");
    tableContainer.removeChild(pageChanger);
    curPageDecrease();
  } else if (students.length % sizeOfPage == 0 && students.length != 0) {
    renderPagesNav(Math.trunc(students.length / sizeOfPage));
    curPageDecrease();
  }
  renderTable();
};

const callDeletionForm = (event) => {
  const app = document.getElementById("app");
  const old = app.innerHTML;
  const row = event.target.parentNode.parentNode;
  const rows = document.getElementById("student-table").rows;
  indexToDelete =
    Array.prototype.indexOf.call(rows, row) - 1 + sizeOfPage * curPage;
  app.innerHTML = `
    <div class = "backdrop" onclick = "closeForm()"></div>
    <div class = "overlay">
      <header>
        <h2>Warning</h2>
        <button onclick = "closeForm()">x</button>
      </header>
      <div class = "caution">
        <h3>Are you sure you want to delete user ${students[indexToDelete].name}?</h3>
      </div>
      <div class = "actions">
        <button onclick = "deleteStudent(), closeForm()">delete</button>
      </div>
    </div>${old}`;
};

const closeForm = () => {
  indexToDelete = null;
  const app = document.getElementById("app");
  const backdrop = document.getElementsByClassName("backdrop");
  const overlay = document.getElementsByClassName("overlay");
  app.removeChild(backdrop[0]);
  app.removeChild(overlay[0]);
};

const createStudent = () => {
  const group = document.getElementById("group-input");
  const firstName = document.getElementById("first-name-input");
  const lastName = document.getElementById("last-name-input");
  const gender = document.getElementById("gender-input");
  const birthdate = document.getElementById("birthdate-input");
  addStudent(
    group.value,
    firstName.value + " " + lastName.value,
    gender.value,
    birthdate.value,
    "online"
  );
  // const dif = students.length - curPage * sizeOfPage;
  // if (dif <= sizeOfPage) {
  renderTable();
  // }
  if (students.length != 1 && students.length % sizeOfPage == 1) {
    renderPagesNav(Math.trunc(students.length / sizeOfPage) + 1);
  }
};

const curPageDecrease = () => {
  if (curPage != 0) {
    curPage--;
    renderTable();
  }
};

const curPageIncrease = () => {
  if (curPage != Math.trunc(students.length / sizeOfPage)) {
    curPage++;
    renderTable();
  }
};

const pageChangeHandler = (event) => {
  const newPage = +event.target.innerHTML - 1;
  if (curPage != newPage) {
    curPage = +event.target.innerHTML - 1;
    renderTable();
  }
};

const renderPagesNav = (pageAmount) => {
  console.log("rendered nav");
  const gridContainer = document.getElementsByClassName("grid-container")[0];
  const pageChanger = document.getElementById("page-changer");
  if (pageChanger) {
    gridContainer.removeChild(pageChanger);
  }
  const navigation = document.createElement("div");
  navigation.id = "page-changer";
  let buttons = `<button onclick = "curPageDecrease()">${"<"}</button>`;
  for (let i = 1; i <= pageAmount; i++) {
    buttons += `
      <button onclick = "pageChangeHandler(event)">${i}</button>
    `;
  }
  buttons += `<button onclick = "curPageIncrease()">${">"}</button>`;
  navigation.innerHTML = buttons;
  gridContainer.appendChild(navigation);
};

const callAnimation = (event) => {
  const bell = event.target;
  bell.classList.add("jingle-bell");
  const dot = document.getElementsByClassName("red-dot")[0];
  dot.classList.add("red-dot-appear");
};

const messageChecked = (event) => {
  const bell = event.target;
  bell.classList.remove("jingle-bell");
  const dot = document.getElementsByClassName("red-dot")[0];
  dot.classList.remove("red-dot-appear");
};
