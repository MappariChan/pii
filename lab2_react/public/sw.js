self.addEventListener("install", (event) => {
  console.log("[Service Worker] Instaling Service Worker ...", event);
  event.waitUntil(
    caches.open("static").then((cache) => {
      console.log("[Service Worker] Adding to the cache ...");
      cache.addAll(["/", "/index.html", "/style.css", "/static/js/bundle.js"]);
    })
  );
});

//comment

self.addEventListener("activate", (event) => {
  console.log("[Service Worker] Activating Service Worker ...", event);
  return self.clients.claim();
});

self.addEventListener("fetch", (event) => {
  event.respondWith(
    caches.match(event.request).then((response) => {
      if (response) {
        return response;
      } else {
        return fetch(event.request);
      }
    })
  );
});
