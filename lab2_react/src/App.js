import React from "react";

import StudentPage from "./Components/StudentPage";
import StudentContextProvider from "./Store/StudentContextProvider";
import Header from "./Components/Header";
import PageNavigation from "./Components/PageNavigation";

import classes from "./App.module.css";

const App = () => {
  return (
    <React.Fragment>
      <Header />
      <div className={classes.content}>
        <PageNavigation />
        <StudentContextProvider>
          <StudentPage />
        </StudentContextProvider>
      </div>
    </React.Fragment>
  );
};

export default App;

//"start": "node src/scraper/index.js && react-scripts start",
