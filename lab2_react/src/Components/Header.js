import classes from "./Header.module.css";

const Header = () => {
  return (
    <header className={classes.appHeader}>
      <h1>CMS</h1>
    </header>
  );
};

export default Header;
