const PageChanger = (props) => {
  const pages = [];

  const buttonClickHandler = (event) => {
    props.onChangePage(+event.target.textContent);
  };

  for (let i = 1; i <= props.amount; i++) {
    pages.push(<button onClick={buttonClickHandler}>{`${i}`}</button>);
  }

  return (
    <div>
      <button onClick={props.onPrevPage}>{"<"}</button>
      {pages}
      <button onClick={props.onNextPage}>{">"}</button>
    </div>
  );
};

export default PageChanger;
