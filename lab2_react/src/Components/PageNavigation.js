import classes from "./PageNavigation.module.css";

const PageNavigation = () => {
  return (
    <nav className={classes.navigation}>
      <ul>
        <li>
          <a href="#">First Page</a>
        </li>
        <li>
          <a href="#">Second Page</a>
        </li>
        <li>
          <a href="#">Third Page</a>
        </li>
      </ul>
    </nav>
  );
};

export default PageNavigation;
