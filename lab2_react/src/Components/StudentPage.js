import React, { useState, useContext } from "react";
import ReactDOM from "react-dom";

import StudentTable from "./StudentTable";
import AddModal from "../UI/AddModal";
import Backdrop from "../UI/Backdrop";
import StudentContext from "../Store/StudentContext";

import classes from "./StudentPage.module.css";

const StudentPage = () => {
  const [isCreateForm, setIsCreateForm] = useState(false);
  const studentsCtx = useContext(StudentContext);

  const callAddFormHandler = () => {
    setIsCreateForm(true);
  };

  const closeAddFormHandler = () => {
    setIsCreateForm(false);
  };

  return (
    <div className={classes["student-page-container"]}>
      <h2>Students</h2>
      <button onClick={callAddFormHandler}>+</button>
      <StudentTable />
      {isCreateForm &&
        ReactDOM.createPortal(
          <AddModal
            onAdd={studentsCtx.addStudent}
            onCloseModal={closeAddFormHandler}
            type="Add"
          />,
          document.getElementById("modal")
        )}
      {isCreateForm &&
        ReactDOM.createPortal(
          <Backdrop onClick={closeAddFormHandler} />,
          document.getElementById("backdrop")
        )}
    </div>
  );
};

export default StudentPage;
