import React, { useState, useContext } from "react";

import StudentTableRow from "./StudentTableRow";
import StudentContext from "../Store/StudentContext";
import PageChanger from "./PageChanger";

import classes from "./StudentTable.module.css";

const StudentTable = () => {
  console.log("rendered");

  const [currentPage, setCurrentPage] = useState(0);
  const sizeOfPage = 5;
  const studentsCtx = useContext(StudentContext);

  console.log(studentsCtx.students);

  const students = studentsCtx.students;
  const firstStudentOfThePage = currentPage * sizeOfPage;
  const lastStudentOfThePage =
    students.length > firstStudentOfThePage + sizeOfPage
      ? firstStudentOfThePage + sizeOfPage
      : students.length;
  if (
    firstStudentOfThePage == lastStudentOfThePage &&
    firstStudentOfThePage != 0
  ) {
    console.log("Deleted Last Student of the Page!");
    setCurrentPage((prev) => prev - 1);
  }

  const amount = Math.ceil(students.length / sizeOfPage);

  const setPageHandler = (page) => {
    if (page - 1 != currentPage) {
      setCurrentPage(page - 1);
    }
  };

  const setPrevPageHandler = () => {
    if (currentPage != 0) {
      setCurrentPage((prev) => prev - 1);
    }
  };

  const setNextPageHandler = () => {
    if (currentPage != amount - 1) {
      setCurrentPage((prev) => prev + 1);
    }
  };

  return (
    <React.Fragment>
      <div className={classes["student-table-container"]}>
        <table className={classes["student-table"]}>
          <thead>
            <tr>
              <th></th>
              <th>Group</th>
              <th>Full Name</th>
              <th>Sex</th>
              <th>Birthdate</th>
              <th>Status</th>
              <th>Options</th>
            </tr>
          </thead>
          <tbody>
            {students
              .slice(firstStudentOfThePage, lastStudentOfThePage)
              .map((student) => (
                <StudentTableRow
                  key={student.id}
                  student={student}
                  onDelete={studentsCtx.removeStudent}
                  onEdit={studentsCtx.editStudent}
                />
              ))}
          </tbody>
        </table>
      </div>
      {amount > 1 && (
        <PageChanger
          amount={amount}
          onChangePage={setPageHandler}
          onPrevPage={setPrevPageHandler}
          onNextPage={setNextPageHandler}
        />
      )}
    </React.Fragment>
  );
};

export default StudentTable;
