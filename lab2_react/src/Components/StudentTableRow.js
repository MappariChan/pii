import React, { useState } from "react";
import ReactDOM from "react-dom";

import DeleteModal from "../UI/DeleteModal";
import Backdrop from "../UI/Backdrop";
import AddModal from "../UI/AddModal";

import classes from "./StudentTableRow.module.css";

const StudentTableRow = (props) => {
  const [isDeleteForm, setIsDeleteForm] = useState(false);
  const [isEditForm, setIsEditForm] = useState(false);

  const student = props.student;

  const deleteHandler = () => {
    props.onDelete(student.id);
  };

  const editHandler = (edited) => {
    props.onEdit(student.id, edited);
  };

  const closeDeleteFormHandler = () => {
    setIsDeleteForm(false);
  };
  const callDeleteFormHandler = () => {
    setIsDeleteForm(true);
  };
  const closeEditFormHandler = () => {
    setIsEditForm(false);
  };
  const callEditFormHandler = () => {
    setIsEditForm(true);
  };
  const closeFormHandler = () => {
    setIsDeleteForm(false);
    setIsEditForm(false);
  };

  const fullName = student.name + " " + student.surname;
  // const date = props.birthdate;
  // const year = date.getFullYear();
  // const month = date.getMonth() + 1;
  // const day = date.getDate();
  // const formattedMonth = month < 10 ? `0${month}` : String(month);
  // const formatedDate = `${day}.${formattedMonth}.${year}`;

  return (
    <tr className={classes["student-table-row"]}>
      <td>
        <input type="checkbox"></input>
      </td>
      <td>{student.group.name}</td>
      <td>{fullName}</td>
      <td>{student.sex}</td>
      <td>{student.birthdate.toISOString().slice(0, 10)}</td>
      <td>{student.status}</td>
      <td>
        <div className={classes["student-options"]}>
          <button onClick={callEditFormHandler}>/</button>
          <button onClick={callDeleteFormHandler}>x</button>
        </div>
      </td>
      {isDeleteForm &&
        ReactDOM.createPortal(
          <DeleteModal
            onClose={closeDeleteFormHandler}
            onDelete={deleteHandler}
            fullName={fullName}
          />,
          document.getElementById("modal")
        )}
      {isEditForm &&
        ReactDOM.createPortal(
          <AddModal
            onCloseModal={closeEditFormHandler}
            onEdit={editHandler}
            {...student}
            type="Edit"
          />,
          document.getElementById("modal")
        )}
      {(isDeleteForm || isEditForm) &&
        ReactDOM.createPortal(
          <Backdrop onClick={closeFormHandler} />,
          document.getElementById("backdrop")
        )}
    </tr>
  );
};

export default StudentTableRow;
