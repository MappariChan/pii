import React from "react";

const StudentContext = React.createContext({
  students: [],
  addStudent: () => {},
  removeStudent: () => {},
  editStudent: () => {},
});

export default StudentContext;
