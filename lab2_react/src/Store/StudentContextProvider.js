import React, { useState, useEffect } from "react";

import StudentConext from "./StudentContext";

//import parseTitle from "../Parse";

import data from "../data/data.json";

const StudentContextProvider = (props) => {
  const [students, setStudents] = useState([]);

  useEffect(() => {
    // const stringValue = localStorage.getItem("arrayOfStudents");
    // if (stringValue) {
    //   const notProcessedData = JSON.parse(stringValue);
    //   setStudents(
    //     notProcessedData.map((item) => {
    //       return {
    //         ...item,
    //         birthdate: new Date(item.birthdate.substring(0, 10)),
    //       };
    //     })
    //   );
    // }
    fetch("http://localhost:8000/api/v1/students")
      .then((response) => response.json())
      .then((data) => {
        setStudents(() => {
          const newdata = data.map((item) => ({
            ...item,
            birthdate: new Date(item.birthdate.substring(0, 10)),
          }));
          for (let el of newdata) {
            el["group"] = {
              name: el["groupName"],
              speciality: el["speciality"],
              institute: el["institute"],
            };
            delete el["groupName"];
            delete el["speciality"];
            delete el["institute"];
            console.log(el["group"]);
          }
          return newdata;
        });
      })
      .catch((error) => console.error(error));
  }, []);

  // useEffect(() => {
  //   const stringValue = JSON.stringify(students);
  //   localStorage.setItem("arrayOfStudents", stringValue);
  // }, [students]);

  const addStudentHandler = (student) => {
    setStudents((prev) => [student, ...prev]);
    const stringValue = JSON.stringify(student);
    console.log(stringValue);
  };

  const removeStudentHandler = (id) => {
    setStudents((prev) => prev.filter((student) => student.id != id));
    fetch(`http://localhost:8000/api/v1/students/${id}`, {
      method: "DELETE",
    });
  };
  const editStudentHandler = async (id, edited) => {
    const prevStudent = students.filter((student) => student.id == id)[0];
    await fetch(`http://localhost:8000/api/v1/students/${id}`, {
      method: "PUT",
      body: JSON.stringify(edited),
      headers: { "content-type": "application/json" },
    }).catch((error) => console.error(error));
    const indexOfPrevStudent = students.indexOf(prevStudent);
    console.log(edited);
    edited["group"] = {
      name: edited.groupName,
      speciality: edited.speciality,
      institute: edited.institute,
    };
    edited.birthdate = new Date(edited.birthdate);
    console.log(edited, "________");
    setStudents((prev) => {
      const temp = [...prev];
      temp[indexOfPrevStudent] = { id: id, ...edited };
      return temp;
    });
  };

  return (
    <StudentConext.Provider
      value={{
        students: students,
        addStudent: addStudentHandler,
        removeStudent: removeStudentHandler,
        editStudent: editStudentHandler,
      }}
    >
      {props.children}
    </StudentConext.Provider>
  );
};

export default StudentContextProvider;
