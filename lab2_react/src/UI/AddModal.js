import React, { useState, useEffect, useReducer } from "react";
import classes from "./Modal.module.css";
import data from "../data/data.json";

import GroupSelector from "./GroupSelector";
import { act } from "react-dom/test-utils";

const isLettersOnly = (str) => {
  return /^[a-zA-Z]+$/.test(str);
};

const nameReducer = (state, action) => {
  if (action.type == "INPUT_EDIT") {
    let isValid = false;
    if (
      action.value.length > 2 &&
      action.value.length <= 20 &&
      isLettersOnly(action.value)
    ) {
      isValid = true;
    }
    console.log(isValid);
    return {
      value: action.value,
      isValid: isValid,
    };
  }
  return {
    value: "",
    isValid: false,
  };
};

const AddModal = (props) => {
  const [group, setGroup] = useState(
    props.group ? props.group : { name: "", speciality: "", institute: "" }
  );
  console.log("[AddModal] rendered");

  const [name, dispatchName] = useReducer(
    nameReducer,
    props.name
      ? { value: props.name, isValid: true }
      : { value: "", isValid: undefined }
  );
  // const [name, setName] = useState(props.name ? props.name : "");
  const [surname, dispatchSurname] = useReducer(
    nameReducer,
    props.surname
      ? { value: props.surname, isValid: true }
      : { value: "", isValid: undefined }
  );
  // const [surname, setSurname] = useState(props.surname ? props.surname : "");
  const [sex, setSex] = useState(props.sex ? props.sex : "");
  const [birthdate, setBirthdate] = useState(
    props.birthdate ? props.birthdate : new Date("2004-03-29")
  );

  const addStudentHandler = async (event) => {
    event.preventDefault();
    const student = {
      groupName: group.name,
      speciality: group.speciality,
      institute: group.institute,
      name: name.value,
      surname: surname.value,
      sex: sex,
      birthdate: birthdate.toISOString().substring(0, 10),
      status: "ONLINE",
    };

    if (props.type == "Edit") {
      console.log(JSON.stringify(student));
      console.log(props.id);
      console.log(student);
      await props.onEdit(student);
      props.onCloseModal();
    } else {
      fetch("http://localhost:8000/api/v1/students", {
        method: "POST",
        body: JSON.stringify(student),
        headers: { "content-type": "application/json" },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          const student = data.data;
          student["group"] = {
            name: student.groupName,
            speciality: student.speciality,
            institute: student.institute,
          };
          student.birthdate = new Date(student.birthdate);
          props.onAdd(data.data);
          props.onCloseModal();
        })
        .catch((error) => console.log(error));
    }
    //
  };

  const nameChangeHandler = (event) => {
    //setName(event.target.value);
    dispatchName({ type: "INPUT_EDIT", value: event.target.value });
  };

  const surnameChangeHandler = (event) => {
    dispatchSurname({ type: "INPUT_EDIT", value: event.target.value });
  };

  const sexChangeHandler = (event) => {
    setSex(event.target.value);
  };

  const dateChangeHandler = (event) => {
    setBirthdate(new Date(event.target.value));
  };

  return (
    <form className={classes.modal} onSubmit={addStudentHandler}>
      <header>
        <h2>{props.type} Student</h2>
        <button onClick={props.onCloseModal}>x</button>
      </header>
      <div className={`${classes["add-form-grid"]} ${classes.info}`}>
        <label>Group</label>
        <GroupSelector onChange={setGroup} value={group} />
        <label htmlFor="name-input">Name</label>
        <input
          id="name-input"
          onChange={nameChangeHandler}
          value={name.value}
          className={
            name.isValid == false ? classes["invalid-input-value"] : ""
          }
        ></input>
        <label htmlFor="surname-input">Surname</label>
        <input
          id="surname-input"
          onChange={surnameChangeHandler}
          value={surname.value}
          className={
            surname.isValid == false ? classes["invalid-input-value"] : ""
          }
        ></input>
        <label htmlFor="sex-input">Sex</label>
        <input id="sex-input" onChange={sexChangeHandler} value={sex}></input>
        <label htmlFor="birthdate-input">Birthdate</label>
        <input
          id="birthdate-input"
          onChange={dateChangeHandler}
          type="date"
          value={birthdate.toISOString().slice(0, 10)}
          max="2006-12-31"
          min="1998-01-01"
        ></input>
      </div>
      <footer className={classes["modal-navigation"]}>
        <button type="submit">{props.type}</button>
        <button onClick={props.onCloseModal}>Close</button>
      </footer>
    </form>
  );
};

export default AddModal;
