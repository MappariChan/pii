import classes from "./Modal.module.css";

const DeleteModal = (props) => {
  const onDelete = () => {
    props.onDelete();
    props.onClose();
  };

  return (
    <div className={classes.modal}>
      <header>
        <h2>Delete Student</h2>
        <button onClick={props.onClose}>x</button>
      </header>
      <div className={classes.info}>
        <p>Are you sure you want to delete user {props.fullName}?</p>
      </div>
      <footer className={classes["modal-navigation"]}>
        <button onClick={onDelete}>Delete</button>
        <button onClick={props.onClose}>Close</button>
      </footer>
    </div>
  );
};

export default DeleteModal;
