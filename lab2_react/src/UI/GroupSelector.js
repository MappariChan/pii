import React, { useState, useEffect } from "react";

import data from "../data/data.json";

const getGroups = (specialityName) => {
  const groups = [];
  for (let i = 1; i <= 4; i++) {
    for (let j = 1; j <= 4; j++) {
      groups.push(`${specialityName}-${10 * i + j}`);
    }
  }
  return groups;
};

const GroupSelector = (props) => {
  console.log(props.value);
  const [institutes] = useState(data.map((item) => item.name));
  const [specialities, setSpecialities] = useState(
    props.value.name != ""
      ? data.filter((item) => item.name == props.value.institute)[0]
          .specialityList
      : []
  );
  const [groups, setGroups] = useState(
    props.value.name != "" ? getGroups(props.value.speciality) : []
  );

  useEffect(() => {
    if (props.value.name != "") {
      document.getElementById("institute-selector").value =
        props.value.institute;
      document.getElementById("speciality-selector").value =
        props.value.speciality;
      document.getElementById("group-selector").value = props.value.name;
    }
  }, []);

  const institutesChangeHandler = (event) => {
    const selectedInstitute = event.target.selectedOptions[0].value;
    const specialitiesOfSelectedInstitute = data.filter(
      (item) => item.name == selectedInstitute
    )[0].specialityList;
    setSpecialities(specialitiesOfSelectedInstitute);
    document.getElementById("speciality-selector").value = "default";
    setGroups([]);
  };

  const specialitiesChangeHandler = (event) => {
    const name = event.target.selectedOptions[0].value;
    setGroups(getGroups(name));
    document.getElementById("group-selector").value = "default";
  };

  const groupChnageHandler = (event) => {
    const group = {
      name: document.getElementById("group-selector").value,
      speciality: document.getElementById("speciality-selector").value,
      institute: document.getElementById("institute-selector").value,
    };
    console.log(group);
    props.onChange(group);
  };

  return (
    <div>
      <select
        onChange={institutesChangeHandler}
        id="institute-selector"
        className={props.clasName}
      >
        <option selected>Select Institute</option>
        {institutes.map((item) => (
          <option value={item}>{item}</option>
        ))}
      </select>
      <select
        onChange={specialitiesChangeHandler}
        id="speciality-selector"
        className={props.clasName}
      >
        {specialities.length > 0 && (
          <option selected value="default">
            Select Speciality
          </option>
        )}
        {specialities.map((item) => (
          <option value={item}>{item}</option>
        ))}
      </select>
      <select
        onChange={groupChnageHandler}
        id="group-selector"
        className={props.clasName}
      >
        {groups.length > 0 && (
          <option selected value="default">
            Select Group
          </option>
        )}
        {groups.map((item) => (
          <option value={item}>{item}</option>
        ))}
      </select>
    </div>
  );
};

export default GroupSelector;
