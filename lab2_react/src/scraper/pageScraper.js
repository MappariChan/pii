const scraperObject = {
  url: "https://lpnu.ua/institutes",
  async scraper(browser) {
    const scrappedData = [];
    let page = await browser.newPage();
    console.log(`Navigating to ${this.url}...`);
    await page.goto(this.url);
    console.log(await page.title());
    await page.hover(".institutes.first-level");
    const urls = await page.$$eval(
      ".institutes.first-level ~ .dropdown-menu > li",
      (listItems) => {
        listItems = listItems.map((item) => item.querySelector("a").href);
        return listItems;
      }
    );
    console.log(urls);

    const specialityPromise = (link, instituteObj) =>
      new Promise(async (resolve, reject) => {});

    const pagePromise = (link) =>
      new Promise(async (resolve, reject) => {
        const instituteObj = {};
        const newPage = await browser.newPage();
        await newPage.setDefaultNavigationTimeout(100000);
        await newPage.goto(link);
        instituteObj["name"] = await newPage.$eval(
          "h3.title-menu-block",
          (item) => {
            return item.textContent;
          }
        );
        const specialityList = await newPage.$$eval(
          ".view-group-subgroups div.item-list ul li",
          (listItems) => {
            listItems = listItems
              .map((item) => item.querySelector("div div a strong").innerText)
              .filter((item) => item.length != 0);
            return listItems;
          }
        );
        instituteObj["specialityList"] = specialityList;
        await newPage.close();
        resolve(instituteObj);
      });

    for (link of urls) {
      const instituteObj = await pagePromise(link);
      scrappedData.push(instituteObj);
      console.log(instituteObj);
    }

    return scrappedData;
  },
};

module.exports = scraperObject;
