<?php
    $students_db = pg_connect("host=localhost port=5432 user=postgres password=290320044773 dbname=postgres");
    if($student_db)
    {
        echo "Connected";
    }
    else
    {
        echo "Not connected";
    }
    header('Access-Control-Allow-Origin: *');
    $request_body = file_get_contents('php://input');
    $student = json_decode($request_body, true);
    $student_name_length = strlen($student["name"]);
    $name_is_valid = true;
    if($student_name_length < 3 || $student_name_length > 20)
    {
        $name_is_valid = false;
    }
    $student_surname_length = strlen($student["surname"]);
    $surname_is_valid = true;
    if($student_surname_length < 3 || $student_surname_length > 20)
    {
        $surname_is_valid = false;
    }
    $group = $student["group"]["name"];
    $group_is_valid = true;
    if($group == "")
    {
        $group_is_valid = true;
    }
    $sex = $student["sex"];
    $sex_is_valid = true;
    if($sex == "")
    {
        $sex_is_valid = false;
    }
    $response = [
        "isValid" => ($name_is_valid && $surname_is_valid && $group && $sex),
    ];
    echo json_encode($response);
?>