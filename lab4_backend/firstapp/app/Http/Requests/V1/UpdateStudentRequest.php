<?php

namespace App\Http\Requests\V1;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $method = $this->method();
        if($method == "PUT"){
            return [
                "name" => ["required"],
                "surname" => ["required"],
                "groupName" => ["required"],
                "speciality" => ["required"],
                "institute" => ["required"],
                "birthdate" => ["required"],
                "sex" => ["required"],
                "status" => ["required"],
            ];
        } else {
            return [
                "name" => ["sometimes, required"],
                "surname" => ["sometimes, required"],
                "groupName" => ["sometimes, required"],
                "speciality" => ["sometimes, required"],
                "institute" => ["sometimes, required"],
                "birthdate" => ["sometimes, required"],
                "sex" => ["sometimes, required"],
                "status" => ["sometimes, required"],
            ];
        }
    }
}
