<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Student>
 */
class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $status = $this->faker->randomElement(["ON", "OFF"]);
        $sex = $this->faker->randomElement(["M", "F"]);

        return [
            "name" => $this->faker->name(),
            "surname" => $this->faker->name(),
            "groupName" => $this->faker->name(),
            "speciality" => $this->faker->name(),
            "institute" => $this->faker->name(),
            "birthdate" => $this->faker->dateTimeThisDecade()->format("Y-m-d"),
            "sex" => $sex,
            "status" => $status,
        ];
    }
}
