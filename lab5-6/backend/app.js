require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const ioManager = require("./socket");

const userRoute = require("./routes/user");
const chatRoute = require("./routes/chats");
const authMiddleware = require("./controllers/authMiddleware");
const { onRead } = require("./controllers/chat");

const app = express();

app.use(cors());
app.use(express.json());

app.use(userRoute);

app.use("/chats", authMiddleware);
app.use("/chats", chatRoute);

mongoose
  .connect(process.env.CONNECTION_KEY)
  .then((result) => {
    const server = app.listen(8000);
    const io = ioManager.init(server);
    io.on("connection", (socket) => {
      // socket.on("readed", onRead);
    });
  })
  .catch((error) => {
    console.log(error);
  });
