const jwt = require("jsonwebtoken");
const User = require("../models/user");

module.exports = authMiddleware = async (req, res, next) => {
  try {
    const token = req.header("Authorization").split(" ")[1];
    if (!token) {
      return res.status(400).json({ message: "Token not valid" });
    }
    const decodedData = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = await User.findOne({ email: decodedData.email });
    next();
  } catch (e) {
    console.log(e);
    return res.status(400).json({ message: "Token not valid" });
  }
};
