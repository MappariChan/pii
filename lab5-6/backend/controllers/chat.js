const Chat = require("../models/chat");
const User = require("../models/user");
const ioManager = require("../socket");

module.exports.getAllChats = async (req, res, next) => {
  const chats = await Chat.find({
    members: { $elemMatch: { userId: req.user._id } },
  })
    .populate("members.userId")
    .populate("messages.userId")
    .populate("messages.isReaded.userId")
    .exec()
    .catch((error) => console.error(error));
  res.send(chats);
};

module.exports.addChat = async (req, res, next) => {
  console.log(req.body.name);
  const user = await User.findOne({ login: req.body.name }).catch((error) => {
    console.error(error);
  });
  if (!user) {
    return res
      .status(400)
      .json({ message: "There is no user with such name!" });
  }
  const chatObject = {
    title: req.body.title,
    members: [{ userId: req.user._id }, { userId: user._id }],
    messages: [],
  };
  const chat = new Chat(chatObject);
  chat
    .save()
    .then(async () => {
      const populatedChat = await Chat.populate(chat, "members.userId");
      ioManager.get().emit("chat_creation", {
        action: "CREATE",
        chat: populatedChat,
      });
      return res.json({ message: "Chat was successfuly added!" });
    })
    .catch((error) => {
      console.error(error);
      return res.status(400).json({ message: "Invalid operation!" });
    });
};

module.exports.getChat = async (req, res, next) => {
  await Chat.findOne({ _id: req.params.id })
    .populate("members.userId")
    .populate("messages.userId")
    .exec()
    .then((chat) => {
      res.json(chat);
    })
    .catch((error) => {
      console.error(error);
      res.status(400).json({ message: "Invalid chat id!" });
    });
};

module.exports.sendMessage = async (req, res, next) => {
  const body = req.body;
  const chat = await Chat.findOne({ _id: req.params.id });
  const isReaded = chat.members.map((member) => {
    return {
      userId: member.userId,
      value: false,
    };
  });
  await Chat.findByIdAndUpdate(
    req.params.id,
    {
      $push: {
        messages: {
          userId: req.user._id,
          text: body.message,
          isReaded: isReaded,
        },
      },
    },
    { new: true }
  )
    .populate("messages.userId")
    .exec()
    .then((updatedChat) => {
      console.log(updatedChat);
      ioManager.get().emit("message_send", {
        action: "SEND",
        chatId: updatedChat._id,
        message: updatedChat.messages.slice(-1)[0],
      });
      res.json(updatedChat);
    })
    .catch((error) => {
      console.error(error);
      return res.status(400).json({ message: "Invalid operation!" });
    });
};

module.exports.addUser = async (req, res, next) => {
  const user = await User.findOne({
    $or: [{ email: req.body.name }, { login: req.body.name }],
  });
  await Chat.findByIdAndUpdate(
    req.params.id,
    {
      $push: { members: { userId: user._id } },
    },
    { new: true }
  )
    .populate("members.userId")
    .populate("messages.userId")
    .exec()
    .then((updatedChat) => {
      console.log(updatedChat);
      ioManager.get().emit("adding_to_group", {
        action: "ADD",
        chat: updatedChat,
      });
      res.json(updatedChat);
    })
    .catch((error) => {
      console.error(error);
      return res.status(400).json({ message: "Invalid operation!" });
    });
};

// module.exports.onRead = async (data) => {
//   const user = await User.findOne({
//     $or: [{ email: data.login }, { login: data.login }],
//   });
//   const chat = await Chat.findOne({ _id: data.chatId });
//   chat.messages
//     .filter((msg) => msg._id == data.messageId)[0]
//     .isReaded.filter(
//       (item) => item.userId.toString() == user._id.toString()
//     )[0].value = true;
//   await chat.save();
// };
