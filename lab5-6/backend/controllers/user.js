const jsonwebtoken = require("jsonwebtoken");

const User = require("../models/user");

const isExist = async (user) => {
  return await User.findOne({
    $or: [{ email: user.email }, { login: user.login }],
  }).then((user) => {
    if (user) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.registration = userRegistartion = async (req, res, next) => {
  const user = new User({ ...req.body });
  if (await isExist(user)) {
    const result = {
      message: "User with such email or login exists!",
    };
    res.status(400).send(JSON.stringify(result));
    return;
  }
  user
    .save()
    .then(() => {
      const token = {
        token: jsonwebtoken.sign(
          { email: user.email },
          process.env.TOKEN_SECRET,
          {
            expiresIn: "24h",
          }
        ),
      };
      const jsonToken = JSON.stringify(token);
      res.send(jsonToken);
    })
    .catch((error) => {
      console.error(error);
    });
};

module.exports.login = userLogin = (req, res, next) => {
  const user = req.body;
  User.findOne({
    $or: [{ email: user.name }, { login: user.name }],
    password: user.password,
  })
    .then((user) => {
      if (!user) {
        const result = {
          message: "Can't find such user!",
        };
        res.status(400).send(JSON.stringify(result));
        return;
      } else {
        const token = {
          token: jsonwebtoken.sign(
            { email: user.email },
            process.env.TOKEN_SECRET,
            {
              expiresIn: "24h",
            }
          ),
        };
        const jsonToken = JSON.stringify(token);
        res.send(jsonToken);
      }
    })
    .catch((error) => console.error(error));
};
