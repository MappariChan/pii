const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const chatSchema = mongoose.Schema({
  title: { type: String, required: true },
  members: [
    {
      userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
    },
  ],
  messages: [
    {
      userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
      text: {
        type: String,
        required: true,
      },
      isReaded: [
        {
          userId: { type: Schema.Types.ObjectId, required: true, ref: "User" },
          value: { type: Boolean, required: true },
        },
      ],
    },
  ],
});

module.exports = mongoose.model("Chat", chatSchema);
