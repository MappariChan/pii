const express = require("express");
const {
  getAllChats,
  addChat,
  sendMessage,
  getChat,
  addUser,
} = require("../controllers/chat");

const chatRoute = express.Router();

chatRoute.get("/", getAllChats);
chatRoute.post("/", addChat);
chatRoute.param("id", (req, res, next, id) => {
  req.params.id = id;
  next();
});
chatRoute.get("/:id", getChat);
chatRoute.post("/:id", sendMessage);
chatRoute.post("/:id/add-user", addUser);

module.exports = chatRoute;
