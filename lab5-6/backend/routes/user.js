const express = require("express");
const { registration, login } = require("../controllers/user");

const userRoute = express.Router();

userRoute.post("/register", registration);
userRoute.post("/login", login);

module.exports = userRoute;
